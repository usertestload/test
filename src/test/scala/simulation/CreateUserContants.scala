package simulation

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class CreateUserContants extends Simulation {

  val httpProtocol = http.baseUrl(System.getProperty("Host", "https://reqres.in"))
  val createUser = new CreateUser

  setUp(createUser.getCreateUserRequest.inject(atOnceUsers(100)).protocols(httpProtocol))


}