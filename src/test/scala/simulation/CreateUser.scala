package simulation

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._


class CreateUser {

  val dataService = csv("DataUser.csv").random

  val scenarioCreate = scenario("Create user").feed(dataService)
    .exec(http("Create user from Company")
      .post("/api/users")
      .header("Content-Type", "application/json")
      .body(StringBody(
        """
       {
        "name": "${name}",
        "job": "${job}"
       }
      """
      ))
      .check(status is 201))

  def getCreateUserRequest: ScenarioBuilder = scenarioCreate
}